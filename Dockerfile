# Use a imagem oficial do Nginx como base
FROM nginx:latest

# Instalar ping e curl
RUN apt-get update && apt-get install -y iputils-ping curl

# Crie um diretório para as configurações do Nginx
RUN mkdir -p /etc/nginx/tcpconf.d

# Copie o arquivo de configuração para o diretório
COPY nginx.conf /etc/nginx/tcpconf.d/nginx.conf

# Exponha a porta 8080 (ou outra porta, se necessário)
EXPOSE 8080

# Comando para iniciar o Nginx
CMD ["nginx", "-g", "daemon off;"]
